package org.tele.equip.repository;

import java.util.List;

import javax.ejb.Local;

import org.tele.equip.domain.Connector;
import org.tele.equip.domain.Node;

@Local
public interface JpaRepository {
	

	/**
	 * Список узлов
	 * 
	 * @return List<Node>
	 */
	List<Node> getAllNodes();
	
	/**
	 * Список коннекторов
	 * 
	 * @return List<Connector>
	 */
	List<Connector> getAllConnectors();
	
	/**
	 * Общее количество точек на всех коннекторах узла
	 *  
	 * @param nodeId - идентификатор узла  
	 * @return количество точек
	 */
	String getPointsCountForNode(Long nodeId);

	/**
	 * Общее количество свободных точек на всех коннекторах узла
	 *  
	 * @param nodeId - идентификатор узла  
	 * @return количество свободных точек
	 */
	String getFreePointsCountForNode(Long nodeId);
	
	/**
	 * Сохранение узла в БД
	 * 
	 * @param node
	 * @return
	 */
	boolean persistNode(Node node);
	
	
	/**
	 * Подробные данные узла
	 * 
	 * @param id - идентификатор узла
	 * @return
	 */
	Node getNodeDetails(Long id);
}	