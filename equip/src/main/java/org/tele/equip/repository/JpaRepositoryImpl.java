package org.tele.equip.repository;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.tele.equip.domain.Connector;
import org.tele.equip.domain.Node;

/**
 * Реализация репозитария для работы с БД
 *  
 * @author Alex
 *
 */

@Stateless
public class JpaRepositoryImpl implements JpaRepository {

	@PersistenceContext(unitName = "persistence")
	private EntityManager entityManager;

	public List<Node> getAllNodes() {

		return entityManager.createQuery("select e from Node e")
				.getResultList();
	}

	public List<Connector> getAllConnectors() {

		return entityManager.createQuery("select e from Connector e")
				.getResultList();
	}

	public String getPointsCountForNode(Long nodeId) {

		Query query = entityManager
				.createQuery(
						"select count(distinct point.id) from Node node inner join node.connectors connector inner join connector.points point where node.id = :nodeId")
				.setParameter("nodeId", nodeId);

		return query.getSingleResult().toString();
	}

	public String getFreePointsCountForNode(Long nodeId) {

		Query query = entityManager
				.createQuery(
						"select count(distinct point.id) from Node node inner join node.connectors connector inner join connector.points point where node.id = :nodeId and point.pair_point is null")
				.setParameter("nodeId", nodeId);

		return query.getSingleResult().toString();

	}

	public Node getNodeDetails(Long id) {

		Query query = entityManager.createQuery("select e from Node e where e.id =:id")
				.setParameter("id", id);

		return (Node) query.getSingleResult();
	}

	public boolean persistNode(Node node) {

		entityManager.persist(node);

		return true;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
