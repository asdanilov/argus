package org.tele.equip.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Первичный ключ для сущности Point
 *  
 * @author Alex
 *
 */
@Embeddable
public class PointPK implements Serializable {

	private static final long serialVersionUID = -1604954498797515256L;

	private Long id;

	@Column(name="connector_id", insertable=false, updatable=false)
	private Long connectorId;

	public PointPK(){
		super();
	}
	
	public PointPK(Long id, Long connectorId) {
		super();
		this.id = id;
		this.connectorId = connectorId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(Long connectorId) {
		this.connectorId = connectorId;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PointPK)) {
			return false;
		}
		PointPK castOther = (PointPK)other;
		return 
			(this.id == castOther.id)
			&& (this.connectorId == castOther.connectorId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = (int) (hash * prime + this.id);
		hash = (int) (hash * prime + this.connectorId);
		
		return hash;
	}
}
