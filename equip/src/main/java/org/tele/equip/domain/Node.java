package org.tele.equip.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Узел
 *  
 * @author Alex
 *
 */
@Entity
@Table(name = "nodes")
public class Node extends BaseEntity {

	private static final long serialVersionUID = 3008645673643154108L;

	@Column(name = "name")
	private String name;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "street")
	private String street;
	
	@Column(name = "house")
	private String house;

	@OneToMany(mappedBy = "node", cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	List<Connector> connectors;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public List<Connector> getConnectors() {
		return connectors;
	}

	public void setConnectors(List<Connector> connectors) {
		this.connectors = connectors;
	}
	
}
