package org.tele.equip.domain;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Точка коннектора
 *  
 * @author Alex
 *
 */
@Entity
@Table(name = "points")
public class Point implements Serializable{

	private static final long serialVersionUID = -7632005755230830513L;

	@EmbeddedId
	private PointPK id;
	
	@ManyToOne
	@JoinColumn(name="connector_id", insertable=false, updatable=false)
	private Connector connector;

	@OneToOne
	private Point pair_point;

	public Connector getConnector() {
		return connector;
	}

	public void setConnector(Connector connector) {
		this.connector = connector;
	}

	public Point getPair_point() {
		return pair_point;
	}

	public void setPair_point(Point pair_point) {
		this.pair_point = pair_point;
	}

	public PointPK getId() {
		return id;
	}

	public void setId(PointPK id) {
		this.id = id;
	}
	

}
