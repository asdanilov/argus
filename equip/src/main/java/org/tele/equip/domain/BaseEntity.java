package org.tele.equip.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Базовая сущность для генерации id
 *  
 * @author Alex
 *
 */
@MappedSuperclass
public abstract class BaseEntity  implements Serializable {

	private static final long serialVersionUID = 7558231032924727898L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
