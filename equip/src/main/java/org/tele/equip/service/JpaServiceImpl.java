package org.tele.equip.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.tele.equip.domain.Connector;
import org.tele.equip.domain.Node;
import org.tele.equip.repository.JpaRepository;


/**
 * Реализация сервиса для работы с репозитарием
 *  
 * @author Alex
 *
 */

@Stateless
public class JpaServiceImpl implements JpaService {

	@EJB
	private JpaRepository jpaRepository;
	
	public List<Connector> getAllConnectors() {
		
		return jpaRepository.getAllConnectors();
	}

	public List<Node> getAllNodes() {
		
		return jpaRepository.getAllNodes();
	}


	public boolean persistNode(Node node) {
		return jpaRepository.persistNode(node);
	}


	public JpaRepository getJpaRepository() {
		return jpaRepository;
	}


	public void setJpaRepository(JpaRepository jpaRepository) {
		this.jpaRepository = jpaRepository;
	}

	public String getPointsCountForNode(Long nodeId) {
		
		return jpaRepository.getPointsCountForNode(nodeId);
	}

	public String getFreePointsCountForNode(Long nodeId) {
		
		return jpaRepository.getFreePointsCountForNode(nodeId);
	}

	public Node getNodeDetails(Long id) {
		
		return jpaRepository.getNodeDetails(id);
	}

}
