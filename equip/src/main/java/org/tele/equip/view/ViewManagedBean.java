package org.tele.equip.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.tele.equip.domain.Connector;
import org.tele.equip.domain.Node;
import org.tele.equip.domain.Point;
import org.tele.equip.domain.PointPK;
import org.tele.equip.service.JpaService;

/**
 * Бин для взаимодействия с JSF страницами
 *  
 * @author Alex
 *
 */

@ManagedBean
@RequestScoped
public class ViewManagedBean {

	@EJB
	private JpaService jpaService;

	private Long nodeId;

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	
	public JpaService getJpaService() {
		return jpaService;
	}

	public void setJpaService(JpaService jpaService) {
		this.jpaService = jpaService;
	}



	public List<Node> getAllNodes() {
		return getJpaService().getAllNodes();
	}
	
	public Node getNodeDetails() {
		return getJpaService().getNodeDetails(nodeId);
	}

	public String processConnections(Long id){
		return "connections";
	}


	/**
	 * Инициализация БД 
	 */
	
	/*@PostConstruct
	public void init() {
		Node node = new Node();
		node.setName("Петербургский");
		node.setStreet("Московский");
		node.setRegion("СПб");
		node.setHouse("123");

		Node node1 = new Node();
		node1.setName("Московский");
		node1.setStreet("Ленинградская");
		node1.setRegion("Мск");
		node1.setHouse("234");
		
				
		Connector connector1 = new Connector();
		Connector connector2 = new Connector();		
		Connector connector3 = new Connector();
		Connector connector4 = new Connector();
		
		Point p0 = new Point();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Point p5 = new Point();
		Point p6 = new Point();
		Point p7 = new Point();
		Point p8 = new Point();
		Point p9 = new Point();
		Point p10 = new Point();
		Point p11 = new Point();
		
				
		connector1.setName("A");
		connector1.setNode(node);

		connector2.setName("B");
		connector2.setNode(node);
		
		//connector1
		p0.setConnector(connector1);
		p0.setId(new PointPK(0L, 1L));

		p1.setConnector(connector1);
		p1.setPair_point(p2);
		p1.setId(new PointPK(1L, 1L));
		
		p2.setConnector(connector1);
		p2.setPair_point(p1);
		p2.setId(new PointPK(2L, 1L));

		ArrayList<Point> points1 = new ArrayList<Point>();
		points1.add(p0);
		points1.add(p1);
		points1.add(p2);

		
		//connector2
		p3.setConnector(connector2);
		p3.setPair_point(p4);
		p3.setId(new PointPK(0L, 2L));
		
		p4.setConnector(connector2);
		p4.setPair_point(p3);
		p4.setId(new PointPK(1L, 2L));
		
		ArrayList<Point> points2 = new ArrayList<Point>();
		points2.add(p3);
		points2.add(p4);

		ArrayList<Connector> connectors = new ArrayList<Connector>();
		connector1.setPoints(points1);
		connector2.setPoints(points2);
		connectors.add(connector1);
		connectors.add(connector2);

		node.setConnectors(connectors);
		
		
		
		
		
		connector3.setName("C");
		connector3.setNode(node1);
		
		connector4.setName("D");
		connector4.setNode(node1);

		
		//connector3
		p5.setConnector(connector3);
		p5.setId(new PointPK(0L, 3L));

		p6.setConnector(connector3);
		p6.setPair_point(p7);
		p6.setId(new PointPK(1L, 3L));

		p7.setConnector(connector3);
		p7.setPair_point(p6);
		p7.setId(new PointPK(2L, 3L));

		ArrayList<Point> points3 = new ArrayList<Point>();
		points3.add(p5);
		points3.add(p6);
		points3.add(p7);

		
		//connector4
		p8.setConnector(connector4);
		p8.setPair_point(p9);
		p8.setId(new PointPK(0L, 4L));
		
		p9.setConnector(connector4);
		p9.setPair_point(p8);
		p9.setId(new PointPK(1L, 4L));
		
		p10.setConnector(connector4);
		p10.setId(new PointPK(2L, 4L));
		
		p11.setConnector(connector4);
		p11.setId(new PointPK(3L, 4L));
		
		ArrayList<Point> points4 = new ArrayList<Point>();
		points4.add(p8);
		points4.add(p9);
		points4.add(p10);
		points4.add(p11);

		ArrayList<Connector> connectors1 = new ArrayList<Connector>();
		connector3.setPoints(points3);
		connector4.setPoints(points4);
		connectors1.add(connector3);
		connectors1.add(connector4);

		node1.setConnectors(connectors1);
	
		jpaService.persistNode(node);
		jpaService.persistNode(node1);
	}*/
}
